from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///blog.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class Short(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    short_mini = db.Column(db.String(200), nullable=False)
    short_full = db.Column(db.String(200), nullable=False)
    date = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Master %r>' % self.id


class Properties(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    recipe = db.Column(db.String(200), nullable=False)
    country = db.Column(db.String(200), nullable=False)
    date = db.Column(db.DateTime, default=datetime.utcnow)
    short_id = db.Column(db.Integer, db.ForeignKey('short.id'))

    def __repr__(self):
        return '<Master %r>' % self.id


store = ['Дротаверин', 'Омез', 'Ирикар', ':))']  # положить сюда объект products


@app.route('/')
def master():
    products = Short.query.order_by(Short.date.desc()).all()
    return render_template('master.html', store=store, products=products)


@app.route('/short', methods=['POST', 'GET'])
def short():
    if request.method == 'POST':
        short_mini = request.form['short_mini']
        short_full = request.form['short_full']

        shorty = Short(short_mini=short_mini, short_full=short_full)


        recipe = request.form['recipe']
        country = request.form['country']

        db.session.add(shorty)
        db.session.flush()

        properties = Properties(recipe=recipe, country=country, short_id=shorty.id)
        db.session.add(properties)

        try:
            db.session.commit()
            return redirect('/')
        except:
            return 'При добавленни Короткого - произошла ошибка'

    else:
        return render_template('short.html')


@app.route('/form')
def form():
    return render_template('form.html')


if __name__ == '__main__':
    app.run(debug=True)
